# Ansible Configuration

## Initial Configuration

```
# Generate a version lock file of the Python environment configuration
pipenv lock

# Install dependencies for Python environment
pipenv sync

ansible-galaxy install -r requirements.yml
```

## Running the playbook

### Initial deployment

```
ansible-playbook playbook-deploy-omnibus-instance.yml --vault-password-file=keys/vault.pass --inventory=hosts/hosts.yml
```


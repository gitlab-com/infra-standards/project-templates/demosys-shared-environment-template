# gitlabtraining-shared-environment-template

This project template provides an all-in-one configuration of a shared GitLab Omnibus instance, Sidekiq instance, autoscaling GitLab Runner instances, and a Kubernetes cluster for use in training classes.

## Installation Steps

This environment is deployed using GitLab CI in an existing GCP project. See [INSTALL.md](INSTALL.md) for step-by-step build instructions.

## Connecting to the Environment

### GitLab Team Member Access

Each user can visit https://gitlabdemo.cloud (powered by [gitlabdemo-cloud-app](https://gitlab.com/gitlab-com/demo-systems/management-apps/gitlabdemo-cloud-app)) to sign in with Okta and generate credentials using the GitLab API.

### Student Access

TODO

### Root Access

> You must be a member of the `Demo Systems Admins` Google Group and 1Password vault to access the infrastructure.

* GitLab UI Access - [https://TEMPLATE_ENVIRONMENT_FQDN](https://TEMPLATE_ENVIRONMENT_FQDN)
    * 1Password Credentials `Omnibus Root - TEMPLATE_ENVIRONMENT_FQDN`
* GCP Project Console (TEMPLATE_GCP_PROJECT_NAME) - [https://console.cloud.google.com/compute/instances?project=TEMPLATE_GCP_PROJECT_NAME&authuser=0](https://console.cloud.google.com/compute/instances?project=TEMPLATE_GCP_PROJECT_NAME&authuser=0)
* SSH to `TEMPLATE_ENVIRONMENT_UNIQUE_PREFIXgitlab-omnibus-instance`
    ```
    gcloud beta compute ssh --zone "TEMPLATE_GCP_REGION_ZONE" "TEMPLATE_ENVIRONMENT_UNIQUE_PREFIXgitlab-omnibus-instance"  --project "TEMPLATE_GCP_PROJECT_NAME"
    ```
* SSH to `TEMPLATE_ENVIRONMENT_UNIQUE_PREFIXgitlab-runner-manager-instance`
    ```
    `gcloud beta compute ssh --zone "TEMPLATE_GCP_REGION_ZONE" "TEMPLATE_ENVIRONMENT_UNIQUE_PREFIXgitlab-runner-manager-instance"  --project "TEMPLATE_GCP_PROJECT_NAME"
    ```
* Connect to `TEMPLATE_ENVIRONMENT_UNIQUE_PREFIXgitlab-ci-cluster`
    ```
    gcloud container clusters get-credentials TEMPLATE_ENVIRONMENT_UNIQUE_PREFIXgitlab-ci-cluster --region TEMPLATE_GCP_REGION --project TEMPLATE_GCP_PROJECT_NAME
    ```
    ```
    k9s
    ```

## Operational Tasks

> You must be a member of the `Demo Systems Admins` Google Group and 1Password vault to access the infrastructure.

### GitLab Omnibus Instance

#### Enable Feature Flag

1. Connect to the GitLab Omnibus instance using SSH.

1. Enter the `gitlab-rails console` shell.
    ```
    sudo -i
    gitlab-rails console
    ```

1. Check if the feature flag has been enabled.
    ```
    Feature.enabled?(:my_awesome_feature)
    ```
    ```
    => false
    ```

1. Enable the feature flag.
    ```
    > Feature.enable(:my_awesome_feature)
    ```
    ```
    => nil
    ```

### GitLab Runner Manager

#### Get list of runner instances

1. Connect to the Gitlab Runner Manager instance using SSH.

1. Get a list of runner instances.
    ```
    sudo -i
    docker-machine ls
    ```

#### Update configuration of runners

1. Connect to the Gitlab Runner Manager instance using SSH.

1. Modify the `gitlab-runner` configuration.
    ```
    nano /etc/gitlab-runner/config.toml
    ```

#### Redeploy runner instances

1. Connect to the Gitlab Runner Manager instance using SSH.

1. Modify the `gitlab-runner` configuration.
    ```
    nano /etc/gitlab-runner/config.toml
    ```

1. Update the `IdleCount` value to `0` and save the file.

1. Wait a few minutes for the runners to power off. You can monitor this by trailing the logs.
    ```
    tail -f /var/log/syslog
    ```

1. You can check the status of the machines using `docker-machine ls`.
    ```
    docker-machine ls
    ```

1. If needed, you can delete a specific instance.
    ```
    docker-machine kill {machine-name}
    ```

1. Modify the `gitlab-runner` configuration.
    ```
    nano /etc/gitlab-runner/config.toml
    ```

1. Update the `IdleCount` value to `5` (or another value) and save the file.

1. You can monitor the redeployment by trailing the logs.
    ```
    tail -f /var/log/syslog
    ```

### GitLab CI Kubernetes Cluster

See the [k9s key bindings](https://github.com/derailed/k9s#key-bindings) for keyboard usage.

#### View pods for specific namespace

1. Connect to the cluster with `k9s`.

1. Locate the namespace you're looking for. The integer is the GitLab project ID.
    ```
    :namespaces [RETURN]
    ```
    ```
    /12345 [RETURN]
    ```

1. Use up and down arrow keys to highlight the namespace and press `[RETURN]`.

1. See the commands in the top right corner.

#### Delete namespace and clear cluster cache

1. Connect to the cluster with `k9s`.

1. Locate the namespace you're looking for. The integer is the GitLab project ID.
    ```
    :namespaces [RETURN]
    ```
    ```
    /12345 [RETURN]
    ```

1. Use up and down arrow keys to highlight the namespace press `[SPACEBAR]`. Then press `[CTRL] + [D]`. Tab over to `OK` and press `[RETURN]`.

1. In the GitLab UI, navigate to `Admin > Kubernetes` and select the `[prefix]-gitlab-ci-cluster` cluster.

1. Select the `Advanced Settings` tab and click the `Clear cluster cache` button.

1. Re-run the pipeline that had a problem.

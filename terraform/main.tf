# [TEMPLATE_GITLAB_PROJECT_SLUG]/terraform/main.tf

terraform {
  backend "gcs" {
    bucket = "TEMPLATE_GCP_STORAGE_BUCKET_NAME"
    prefix = "terraform/state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
  }
  required_version = ">= 0.13"
}

# Define the Google Cloud Provider
provider "google" {
  project = var.gcp_project
}

# Define the Google Cloud Provider with beta features
provider "google-beta" {
  project = var.gcp_project
}

# Sandbox Environment with VPC, DNS, GitLab Omnibus, and CI Cluster
module "gitlab_omnibus_sandbox" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gitlab/gitlab-omnibus-sandbox-tf-module.git"
  # source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gitlab/gitlab-omnibus-sandbox-tf-module.git?ref=0.1.0"

  env_name                          = var.env_name
  env_prefix                        = var.env_prefix
  env_sandbox_mode                  = var.env_sandbox_mode
  gcp_dns_zone_fqdn                 = var.gcp_dns_zone_fqdn
  gcp_project                       = var.gcp_project
  gcp_region                        = var.gcp_region
  gcp_region_cidr                   = var.gcp_region_cidr
  gcp_region_zone                   = var.gcp_region_zone
  gitlab_omnibus                    = var.gitlab_omnibus
  gitlab_runner_manager             = var.gitlab_runner_manager
  gitlab_ci_cluster                 = var.gitlab_ci_cluster
  gitlab_ci_cluster_ingress         = var.gitlab_ci_cluster_ingress
  gitlab_ci_cluster_letsencrypt_txt = var.gitlab_ci_cluster_letsencrypt_txt
  labels                            = var.labels

}
